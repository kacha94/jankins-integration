//
//  ViewController.swift
//  Jenkins Integration App
//
//  Created by Andrey Polyashev on 17.07.17.
//  Copyright © 2017 PSBJankins. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {

    @IBOutlet fileprivate weak var stackView: UIStackView!
    @IBOutlet fileprivate weak var videoContainer: UIView!
    @IBOutlet fileprivate weak var videoIV: UIImageView!
    @IBOutlet fileprivate weak var recordButton: UIButton!

    var stacks: [UIImageView] = []
    
    var previewLayer: AVCaptureVideoPreviewLayer?
    let captureSession = AVCaptureSession()
    let videoOutput = AVCaptureVideoDataOutput()
    var handler: ((UIImage?) -> Void)?
    var timer: Timer?
    var timerTime: Int = 10
    let fileOutput = AVCaptureMovieFileOutput()
    
    var outputFileUrl: URL {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
        let outputPath = "\(documentsPath)/output.mov"
        return URL(fileURLWithPath: outputPath)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        captureSession.sessionPreset = AVCaptureSessionPresetMedium
        self.videoIV.contentMode = .scaleAspectFit
        
        let camera = AVCaptureDevice.devices().filter({ return ($0 as? AVCaptureDevice)?.position == .front }).first
        let backCamera = camera as? AVCaptureDevice
        
        do {
            let input = try AVCaptureDeviceInput(device: backCamera)
            captureSession.addInput(input)
        } catch {
            print("can't access camera")
            return
        }
        
        videoOutput.setSampleBufferDelegate(self, queue: DispatchQueue.global(qos: .background))
        

//        captureSession.addOutput(fileOutput)
        captureSession.addOutput(videoOutput)

        captureSession.startRunning()
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer?.bounds = CGRect(x: 0, y: 0, width: videoContainer.bounds.width, height: videoContainer.bounds.height)
        previewLayer?.position = CGPoint(x: videoContainer.bounds.midX, y: videoIV.bounds.midY)
        previewLayer?.videoGravity = AVLayerVideoGravityResizeAspect
        videoContainer.layer.addSublayer(previewLayer!)
        
        stackView.addArrangedSubview(videoContainer)
    }
    
    func timerTick() {
        timerTime -= 1
        if timerTime == 6 || timerTime == 3 || timerTime == 0 {
            start()
        }
        if timerTime <= 0 {
            timer?.invalidate()
        }
        self.recordButton.setTitle("\(timerTime)", for: .normal)
        UIView.animate(withDuration: 0.5, animations: { 
            self.recordButton.alpha = 0
        }) { (bool) in
            UIView.animate(withDuration: 0.5, animations: {
                self.recordButton.alpha = 1
            }) { (bool) in
                
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        previewLayer?.bounds = CGRect(x: 0, y: 0, width: videoContainer.bounds.width, height: videoContainer.bounds.height)
        previewLayer?.position = CGPoint(x: videoContainer.bounds.midX, y: videoIV.bounds.midY)
    }
}

extension ViewController: AVCaptureFileOutputRecordingDelegate {
    func capture(_ captureOutput: AVCaptureFileOutput!, didFinishRecordingToOutputFileAt outputFileURL: URL!, fromConnections connections: [Any]!, error: Error!) {
        
    }
}

extension ViewController: AVCaptureVideoDataOutputSampleBufferDelegate {
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputSampleBuffer sampleBuffer: CMSampleBuffer!, from connection: AVCaptureConnection!) {
        let pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer)
        var cameraImage = CIImage(cvPixelBuffer: pixelBuffer!)
        
        let or = UIApplication.shared.statusBarOrientation
        switch or {
        case .landscapeLeft:
            break
        case .landscapeRight:
            break
        case .portrait:
            cameraImage = cameraImage.applyingOrientation(6)
        case .portraitUpsideDown:
            cameraImage = cameraImage.applyingOrientation(8)
        default:
            break
        }

        DispatchQueue.main.async {
            self.videoIV.image = UIImage(ciImage: cameraImage)
            self.handler?(UIImage(ciImage: cameraImage))
        }
    }
    
    @IBAction func addScreenShot(sender: UIButton) {
        try? FileManager.default.removeItem(at: outputFileUrl)
//        fileOutput.startRecording(toOutputFileURL: outputFileUrl as URL!, recordingDelegate: self)
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerTick), userInfo: nil, repeats: true)
        UIView.animate(withDuration: 0.5) { 
            sender.tintColor = UIColor.red
        }
    }
    
    @IBAction func show(sender: UIButton) {
        let item = AVPlayerItem(url: outputFileUrl)
        let player = AVPlayer(playerItem: item)
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.videoGravity = AVLayerVideoGravityResizeAspect
        playerLayer.bounds = stacks.first?.bounds ?? .zero
        stacks.first?.layer.addSublayer(playerLayer)
        
        player.addPeriodicTimeObserver(forInterval: CMTimeMake(1, 2), queue: DispatchQueue.main) { (time) in
            let seconds = CMTimeGetSeconds(time)
            if seconds < 4 {
            
            } else if seconds < 7 {
                playerLayer.removeFromSuperlayer()
                self.stacks[1].layer.addSublayer(playerLayer)
            } else if seconds < 10 {
                playerLayer.removeFromSuperlayer()
                self.stacks[2].layer.addSublayer(playerLayer)
            }
        }
        player.play()
    }
 
    
    func start() {
        
        let imageView = UIImageView(frame: .zero)
        imageView.contentMode = .scaleAspectFit
        imageView.isHidden = true
        
//        let image = UIImage.image(from: previewLayer!)
//        imageView.image = image
        
        handler = { image in
            imageView.image = image
            self.handler = nil
        }

        if stackView.arrangedSubviews.count == 3 {
            self.stacks.append(videoIV)
            fileOutput.stopRecording()
            captureSession.stopRunning()
            return
        }
        stacks.append(imageView)
        stackView.insertArrangedSubview(imageView, at: stackView.arrangedSubviews.count - 1)
        previewLayer?.bounds = CGRect(x: 0, y: 0, width: videoContainer.bounds.width, height: videoContainer.bounds.height)
        previewLayer?.position = CGPoint(x: videoContainer.bounds.midX, y: videoIV.bounds.midY)
        self.view.layoutIfNeeded()
        
        UIView.animate(withDuration: 0.3){
            imageView.isHidden = false
        }
    }
}

extension UIImage {
    class func image(from layer: CALayer) -> UIImage? {
        
        UIGraphicsBeginImageContextWithOptions(layer.bounds.size,
                                               layer.isOpaque, UIScreen.main.scale)
        
        // Don't proceed unless we have context
        guard let context = UIGraphicsGetCurrentContext() else {
            UIGraphicsEndImageContext()
            return nil
        }
        
        layer.render(in: context)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }
}

